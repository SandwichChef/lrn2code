# lrn2code
---

In this repo I'm going to store my personal notes on programming and various practice projects to share with my mentor and friends.

This will read something like a blog, sometimes.

Different projects or perhaps even topics will go in subfolders.

I'll sort the rest of this document later.

# Notes
---

### 20201020
---

This was when I first got bit by the programming bug and actually started to take an interest. I was dissatisfied with the Java Trails, and ended up starting to read Head First Java on Decanter and jdstroy's recommendation.

Although I'd only gotten a few sections in, it actually spurred me to start playing around with an IDE instead of just reading a book, which is more than I could say about anything else I've ever read about programming. 

The things I learned were:
- The necessity of using a psvm to make a test class (in this particular instance, the BicycleTest Class) executable
- How to handle basic output using System.out.println()
- How to handle basic input using a BufferedReader, i.e;
``` Java
        BufferedReader reader = new BufferedReader(
            new InputStreamReader(System.in));

        reader.readLine();
```

- Initially I assumed it would just be System.in.read(), lol. That was funny. After realizing that that expected the next byte of a stream instead, I looked up how to get a string, and after sifting through multiple responses I went with the BufferedReader
- Following that, I managed to intuit how to handle concatenating Strings with other objects. 
    - Then I was informed of the existence of MessageFormat.format("template with parameters {0} and {1}", expression_for_0, expression_for_1);
        - "so rather than concatenating things to the string given to System.out.println, you could do MessageFormat.format("{0}'s bike is faster than {1}'s bike", TestBicycle1.Owner, TestBicycle2.Owner)"
        - I still haven't replaced the occurences of basic System.out.println() in my practice project, but I will sometime soon.
- How to make things comparable by inheriting from the Comparable class and implementing compareTo(), i.e;
``` Java
public class Bicycle implements Comparable<Bicycle> {
     int speed;

 @Override
    public int compareTo(Bicycle other) {
        return(this.speed - other.speed);
    }
```
   
- The reason the implementation is just simple subtraction is the same reason that in math class you heard the result of subtraction referred to as "the difference." If the difference is a positive integer, then your left side (this.speed) is bigger. If the difference is 0, they're equal. If the difference is negative, then the right side (other.speed) is bigger. Maths, how do they work?

### 20201024
---

- Will fill this in later, logs from the other channel on IRC from the morning of the given date

---

# Practice Assignements

### Describe Booru Links (Parsing XML/JSON)
- [ ] Write a Maubot plugin to describe *booru links by parsing either the JSON or XML of a given post 
    - [ ] Identify if something is a *booru link or not
    - [ ] Grab the XML or JSON for a given link
    - [ ] Get and output the tags from that
    - [ ] ????
    - [ ] Profit

- Further to the above project, I want to gain some practical experience serializing/deserializing objects. I can just implement something that does that in the ./practice/ project

### Rock Paper Scissors
- [ ] Rock Paper Scissors (this can also probably be implemented in the ./practice/ project)
    - [ ] Fill in my notes for 20201024
    - [ ] Wrap my head around maths involving modulos, because that's the most idiomatic way to approach the problem
    - [ ] Write the fucking program

### Waifu Combat (Building on serialization/deserialization and the Rock Paper Scissors algorithm)
- [ ] Waifu Combat
    - [ ] Make a class for waifus
        - [ ] It should have at least attributes for names, and the instrument that they play
        - [ ] Write a method for Waifu Combat that expands on the algorithm used in Rock Paper Scissors, but instead of 3 options and mod 3, it'll have five possible options and logic based around mod 5.
    - [ ] Describe Bandori waifus in an XML file
    - [ ] Load the waifus into memory from the XML 
    - [ ] Successful have the waifus engage in combat.


