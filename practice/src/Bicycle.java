public class Bicycle implements Comparable<Bicycle> {
     int speed;
     String owner;
     String colour;

     public void ride() {
         System.out.println(this.owner + " rides his " + this.colour + " bicycle with a swiftness of " + this.speed + " chef units.");
     }

    @Override
    public int compareTo(Bicycle other) {
        return(this.speed - other.speed);
    }
}
