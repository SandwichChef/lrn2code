import java.util.ArrayList;

public class Inventory {
static enum Slot {
HEAD,
BODY,
LEGS,
WEAPON
}
static abstract class InventoryItem {
String name = "";
String description = "";
int weight;
abstract void onUse();
}
static abstract class EquipmentItem extends InventoryItem {
Slot slot = Slot.WEAPON;
int attack;
int defense;
}
static public class potion extends InventoryItem {

void onUse() {
// heal 50 hp code here

}
potion() {
super.name = "Potion";
super.description = "heal 50 hp";
super.weight = 5;
}

}

static public class Sword extends EquipmentItem {

@Override
void onUse() {
// do nothing lmao

}
Sword() {
super.name = "sword";
super.description = "sword";
super.weight = 20;
super.attack = 10;
super.slot = Slot.WEAPON;
}

}
static public class Hat extends EquipmentItem {

@Override
void onUse() {
// do nothing lmao

}
Hat() {
super.name = "hat";
super.description = "hat";
super.weight = 4;
super.defense = 5;
super.slot = Slot.HEAD;
}

}
static ArrayList<InventoryItem> InventoryList = new ArrayList<InventoryItem>();
public static void main(String[] args) {

InventoryList.add(new potion());
InventoryList.add(new Hat());
InventoryList.add(new Sword());
}

}
