import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.MessageFormat;

public class BicycleTest {
    public static void main(String[] args)
            throws IOException
{
        // Enter data using BufferReader
        BufferedReader reader = new BufferedReader(
            new InputStreamReader(System.in));

        Bicycle TestBicycle1 = new Bicycle();
                System.out.println("Who is the owner of the first testBicycle?");
                TestBicycle1.owner = reader.readLine();
                System.out.println("What colour is the first testBicycle?");
                TestBicycle1.colour = reader.readLine();
                System.out.println("How many Chef Units fast is the first testBicycle");
                TestBicycle1.speed = Integer.parseInt(reader.readLine());
                TestBicycle1.ride();
        Bicycle TestBicycle2 = new Bicycle();
                System.out.println("Who is the owner of the second testBicycle?");
                TestBicycle2.owner = reader.readLine();
                System.out.println("What colour is the second testBicycle?");
                TestBicycle2.colour = reader.readLine();
                System.out.println("How many Chef Units fast is the second testBicycle");
                TestBicycle2.speed = Integer.parseInt(reader.readLine());
                TestBicycle2.ride();
                if (TestBicycle1.compareTo(TestBicycle2) > 0)
                        System.out.println(TestBicycle1.owner + "'s bike is faster than" + TestBicycle2.owner + "'s bike");
                else
                        System.out.println(TestBicycle1.owner + "'s bike is not faster than" + TestBicycle2.owner + "'s bike");
    }
}
